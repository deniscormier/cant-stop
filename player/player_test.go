package player_test

import (
	"bitbucket.org/deniscormier/cant-stop/markerstate"
	"bitbucket.org/deniscormier/cant-stop/player"
	"bitbucket.org/deniscormier/cant-stop/stringutil"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Player", func() {

	var pl *player.Player

	BeforeEach(func() {
		pl = player.NewPlayer(1, "Edwin")
	})

	Context("A new Player", func() {

		It("Should have correct id and name", func() {
			Expect(pl.GetID()).To(Equal(1))
			Expect(pl.GetName()).To(Equal("Edwin"))
		})

		It("Should have zero completed tracks", func() {
			Expect(pl.GetCompletedTracks()).To(Equal(stringutil.NewStringSet([]string{})))
		})

		It("Should have all its tracks available", func() {
			Expect(pl.GetAvailableTracks(stringutil.NewStringSet([]string{}))).To(Equal(stringutil.NewStringSet(markerstate.MarkerStateTracks)))
			Expect(pl.GetAvailableTracks(stringutil.NewStringSet([]string{"2", "4", "6", "9", "11"}))).To(Equal(stringutil.NewStringSet([]string{"3", "5", "7", "8", "10", "12"})))
		})

		It("Should have no progress and no turn delta", func() {
			Expect(pl.GetProgressWithoutTurnDelta()).To(Equal(markerstate.ZeroMarkerState))
			Expect(pl.GetProgressTurnDelta()).To(Equal(markerstate.ZeroMarkerState))
		})

	})

	Context("Applying sums on unavailable tracks", func() {

		var sumsAdded []string

		BeforeEach(func() {
			sumsAdded = pl.ApplySums([]string{"3", "4"}, stringutil.NewStringSet([]string{"3", "4"}))
		})

		It("Should not have applied the sums to the tracks", func() {
			Expect(sumsAdded).To(BeNil())
		})

		It("Should have no progress and no turn delta", func() {
			Expect(pl.GetProgressWithoutTurnDelta()).To(Equal(markerstate.ZeroMarkerState))
			Expect(pl.GetProgressTurnDelta()).To(Equal(markerstate.ZeroMarkerState))
		})

	})

	Context("Applying sums on tracks that are available", func() {

		var sumsAdded []string

		BeforeEach(func() {
			sumsAdded = pl.ApplySums([]string{"3", "4"}, stringutil.NewStringSet([]string{"3"}))
		})

		It("Should have zero completed tracks", func() {
			Expect(pl.GetCompletedTracks()).To(Equal(stringutil.NewStringSet([]string{})))
		})

		It("Should have applied the sums to the tracks", func() {
			Expect(sumsAdded).To(Equal([]string{"4"}))
		})

		It("Should still have all tracks available for assignment", func() {
			Expect(pl.GetAvailableTracks(stringutil.NewStringSet([]string{}))).To(Equal(stringutil.NewStringSet(markerstate.MarkerStateTracks)))
		})

		It("Should have no progress, only delta before applying delta to progress", func() {
			Expect(pl.GetProgressWithoutTurnDelta()).To(Equal(markerstate.ZeroMarkerState))
			delta := pl.GetProgressTurnDelta()
			Expect(delta["3"]).To(Equal(0))
			Expect(delta["4"]).To(Equal(1))
		})

		It("Should have no delta, only progress after applying delta to progress", func() {
			pl.ApplyTurnDeltaToProgress()

			progress := pl.GetProgressWithoutTurnDelta()
			Expect(progress["3"]).To(Equal(0))
			Expect(progress["4"]).To(Equal(1))
			Expect(pl.GetProgressTurnDelta()).To(Equal(markerstate.ZeroMarkerState))
		})

		It("Should have no progress and no delta after losing your turn", func() {
			pl.LoseTurn()

			Expect(pl.GetProgressWithoutTurnDelta()).To(Equal(markerstate.ZeroMarkerState))
			Expect(pl.GetProgressTurnDelta()).To(Equal(markerstate.ZeroMarkerState))
		})

	})

	Context("Applying sums to complete a track", func() {

		var sumsAdded []string

		BeforeEach(func() {
			pl.ApplySums([]string{"2"}, stringutil.NewStringSet([]string{}))
			pl.ApplySums([]string{"2"}, stringutil.NewStringSet([]string{}))
			sumsAdded = pl.ApplySums([]string{"2"}, stringutil.NewStringSet([]string{}))
		})

		It("Should have applied the sums to the tracks", func() {
			Expect(sumsAdded).To(Equal([]string{"2"}))
		})

		It("Should not have track \"2\" available for assignment before applying delta to progress", func() {
			Expect(pl.GetAvailableTracks(stringutil.NewStringSet([]string{}))).To(Equal(stringutil.NewStringSet([]string{"3", "4", "5", "6", "7", "8", "9", "10", "11", "12"})))
			Expect(pl.GetAvailableTracks(stringutil.NewStringSet([]string{"3"}))).To(Equal(stringutil.NewStringSet([]string{"4", "5", "6", "7", "8", "9", "10", "11", "12"})))
			Expect(pl.GetAvailableTracks(stringutil.NewStringSet([]string{"2", "3"}))).To(Equal(stringutil.NewStringSet([]string{"4", "5", "6", "7", "8", "9", "10", "11", "12"})))
		})

		It("Should have completed track \"2\" before applying delta to progress", func() {
			Expect(pl.GetCompletedTracks()).To(Equal(stringutil.NewStringSet([]string{"2"})))
		})

		It("Should not have track \"2\" available for assignment after applying delta to progress", func() {
			pl.ApplyTurnDeltaToProgress()
			Expect(pl.GetAvailableTracks(stringutil.NewStringSet([]string{}))).To(Equal(stringutil.NewStringSet([]string{"3", "4", "5", "6", "7", "8", "9", "10", "11", "12"})))
		})

		It("Should have completed track \"2\" after applying delta to progress", func() {
			pl.ApplyTurnDeltaToProgress()
			Expect(pl.GetCompletedTracks()).To(Equal(stringutil.NewStringSet([]string{"2"})))
		})

		It("Should have all tracks available for assignment after losing your turn", func() {
			pl.LoseTurn()
			Expect(pl.GetAvailableTracks(stringutil.NewStringSet([]string{}))).To(Equal(stringutil.NewStringSet(markerstate.MarkerStateTracks)))
		})

		It("Should have no tracks completed after losing your turn", func() {
			pl.LoseTurn()
			Expect(pl.GetCompletedTracks()).To(Equal(stringutil.NewStringSet([]string{})))
		})

	})

	Context("Applying sums on tracks that were available, but they are completed mid-turn", func() {

		var sumsAdded []string

		BeforeEach(func() {
			pl.ApplySums([]string{"2"}, stringutil.NewStringSet([]string{}))
			pl.ApplySums([]string{"2"}, stringutil.NewStringSet([]string{}))
			pl.ApplySums([]string{"2"}, stringutil.NewStringSet([]string{}))
			sumsAdded = pl.ApplySums([]string{"2"}, stringutil.NewStringSet([]string{}))
		})

		It("Should not have applied the sums to the tracks", func() {
			Expect(sumsAdded).To(BeNil())
		})

		It("Should have completed track \"2\" after applying delta to progress", func() {
			pl.ApplyTurnDeltaToProgress()
			Expect(pl.GetCompletedTracks()).To(Equal(stringutil.NewStringSet([]string{"2"})))
		})

	})

	Context("Applying sums on 3 tracks during the same turn", func() {

		var sumsAdded []string

		BeforeEach(func() {
			pl.ApplySums([]string{"2", "3"}, stringutil.NewStringSet([]string{}))
			sumsAdded = pl.ApplySums([]string{"4"}, stringutil.NewStringSet([]string{}))
		})

		It("Should have applied the sums to the tracks", func() {
			Expect(sumsAdded).To(Equal([]string{"4"}))
		})

		It("Should have correct turn delta", func() {
			delta := pl.GetProgressTurnDelta()
			Expect(delta["2"]).To(Equal(1))
			Expect(delta["3"]).To(Equal(1))
			Expect(delta["4"]).To(Equal(1))
		})

		It("Should only have track \"2\", \"3\", and \"4\" available for assignment", func() {
			Expect(pl.GetAvailableTracks(stringutil.NewStringSet([]string{}))).To(Equal(stringutil.NewStringSet([]string{"2", "3", "4"})))
		})

		It("Should only have track \"3\" and \"4\" available for assignment after completing track \"2\"", func() {
			pl.ApplySums([]string{"2"}, stringutil.NewStringSet([]string{}))
			pl.ApplySums([]string{"2"}, stringutil.NewStringSet([]string{}))
			Expect(pl.GetAvailableTracks(stringutil.NewStringSet([]string{}))).To(Equal(stringutil.NewStringSet([]string{"3", "4"})))
		})

	})

	Context("Applying sums on more than 3 tracks during the same turn", func() {

		var sumsAdded []string

		BeforeEach(func() {
			pl.ApplySums([]string{"2", "3"}, stringutil.NewStringSet([]string{}))
			pl.ApplySums([]string{"4"}, stringutil.NewStringSet([]string{}))
			sumsAdded = pl.ApplySums([]string{"5"}, stringutil.NewStringSet([]string{}))
		})

		It("Should not have applied the sum to the track", func() {
			Expect(sumsAdded).To(BeNil())
		})

		It("Should have correct turn delta with \"4\" applied and \"5\" not applied", func() {
			delta := pl.GetProgressTurnDelta()
			Expect(delta["2"]).To(Equal(1))
			Expect(delta["3"]).To(Equal(1))
			Expect(delta["4"]).To(Equal(1))
			Expect(delta["5"]).To(Equal(0))
		})

		It("Should only have track \"2\", \"3\", and \"4\" available for assignment", func() {
			Expect(pl.GetAvailableTracks(stringutil.NewStringSet([]string{}))).To(Equal(stringutil.NewStringSet([]string{"2", "3", "4"})))
		})

	})

	Context("Applying sums: 2 tracks taken, can only assign to 1 more different track", func() {

		var sumsAdded []string

		BeforeEach(func() {
			pl.ApplySums([]string{"2", "3"}, stringutil.NewStringSet([]string{}))
			sumsAdded = pl.ApplySums([]string{"4", "5"}, stringutil.NewStringSet([]string{}))
		})

		It("Should have applied the sum to the track", func() {
			Expect(sumsAdded).To(Equal([]string{"4"}))
		})

		It("Should have correct turn delta with \"4\" applied and \"5\" not applied", func() {
			delta := pl.GetProgressTurnDelta()
			Expect(delta["2"]).To(Equal(1))
			Expect(delta["3"]).To(Equal(1))
			Expect(delta["4"]).To(Equal(1))
			Expect(delta["5"]).To(Equal(0))
		})

		It("Should only have track \"2\", \"3\", and \"4\" available for assignment", func() {
			Expect(pl.GetAvailableTracks(stringutil.NewStringSet([]string{}))).To(Equal(stringutil.NewStringSet([]string{"2", "3", "4"})))
		})
	})

})
