package player

import (
	"bitbucket.org/deniscormier/cant-stop/markerstate"
	"bitbucket.org/deniscormier/cant-stop/stringutil"
)

type Player struct {
	id        int
	name      string
	progress  markerstate.MarkerState
	turnDelta markerstate.MarkerState
}

func NewPlayer(id int, name string) *Player {
	p := new(Player)
	p.id = id
	p.name = name
	p.progress = markerstate.NewMarkerState()
	p.turnDelta = markerstate.NewMarkerState()
	return p
}

func (p *Player) GetID() int {
	return p.id
}

func (p *Player) GetName() string {
	return p.name
}

func (p *Player) GetProgressWithoutTurnDelta() markerstate.MarkerState {
	return p.progress
}

func (p *Player) GetProgressTurnDelta() markerstate.MarkerState {
	return p.turnDelta
}

// Whic tracks have I completed? Takes into account current turn progress.
// Get progress with delta in case we've completed a track mid-turn.
func (p *Player) GetCompletedTracks() stringutil.Set {
	n := markerstate.NewMarkerState()
	n.AddDelta(p.progress)
	n.AddDelta(p.turnDelta)

	return n.GetCompletedTracks()
}

// Given a set of unavailable tracks, which tracks can I still apply sums to?
func (p *Player) GetAvailableTracks(unavailableTracks stringutil.Set) stringutil.Set {
	// Make sure to include mid-turn completed tracks in unavailableTracks
	myCompletedTracks := p.GetCompletedTracks()
	for key := range myCompletedTracks {
		unavailableTracks.Add(key)
	}

	availableTracks := stringutil.Set{}
	numAssignedValues := 0
	assignedAndAvailableTracks := stringutil.Set{}

	for key, deltaValue := range p.turnDelta {
		if !unavailableTracks.Contains(key) {
			// Track is available.
			availableTracks.Add(key)

			// Have we started assigning values to the track?
			if deltaValue > 0 {
				assignedAndAvailableTracks.Add(key)
			}
		}

		if deltaValue > 0 {
			numAssignedValues += 1
		}

	}

	// If we've assigned values to 2 or less tracks this turn.
	//     Return all available tracks.
	// If we've assigned values to 3 tracks this turn.
	//     Return assigned tracks that are still available.
	if numAssignedValues < 3 {
		return availableTracks
	} else {
		//if numAssignedValues > 3 {
		// This is a problem
		//}
		return assignedAndAvailableTracks
	}
}

// Apply values to the tracks. Return sums that were applied to tracks.
// Return nil if we haven't found any values to return.
func (p *Player) ApplySums(sums []string, unavailableTracks stringutil.Set) []string {
	sumsAdded := []string{}

	for _, sum := range sums {
		result := p.applySum(sum, unavailableTracks)
		if result == true {
			sumsAdded = append(sumsAdded, sum)
		}
	}

	if len(sumsAdded) == 0 {
		return nil
	}
	return sumsAdded
}

// Validates whether a sum is allowed to be applied to the track.
// Return whether we applied the sum to the track.
func (p *Player) applySum(sum string, unavailableTracks stringutil.Set) bool {
	// Take into account tracks becoming unavailable mid-turn due to completion or assigning to three tracks in the same turm
	availableTracks := p.GetAvailableTracks(unavailableTracks)

	// Check if track is available
	if !availableTracks.Contains(sum) {
		return false
	}

	// Apply sum to track
	p.turnDelta[sum] += 1
	return true
}

func (p *Player) LoseTurn() {
	p.turnDelta.Reset()
}

// Complete the player's turn
func (p *Player) ApplyTurnDeltaToProgress() {
	p.progress.AddDelta(p.turnDelta)
	p.turnDelta.Reset()
}
