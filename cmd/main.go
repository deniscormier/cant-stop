package main

import (
	"bitbucket.org/deniscormier/cant-stop/scenes"
	"github.com/oakmound/oak"
	"github.com/oakmound/oak/render"
)

func main() {
	oak.LoadConf("oak.config")
	oak.AddScene(scenes.SceneMenu, scenes.MenuInit, scenes.MenuLoop, scenes.MenuEnd)
	oak.AddScene(scenes.SceneGameSetup, scenes.GameSetupInit, scenes.GameSetupLoop, scenes.GameSetupEnd)
	oak.AddScene(scenes.ScenePlay, scenes.PlayInit, scenes.PlayLoop, scenes.PlayEnd)
	render.SetDrawStack(
		render.NewHeap(false),
		render.NewDrawFPS(),
		render.NewLogicFPS(),
	)
	oak.Init(scenes.SceneMenu)
}
