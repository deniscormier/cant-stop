package main

import (
	"bitbucket.org/deniscormier/cant-stop/board"

	"flag"
	"fmt"
	"math/rand"
	"os"
	"time"
)

var isHelp bool

func init() {
	const (
		usage = "Print program usage"
	)
	flag.BoolVar(&isHelp, "help", false, usage)
	flag.BoolVar(&isHelp, "h", false, usage+" (shorthand)")
}

func main() {

	// Initialize random seed
	rand.Seed(time.Now().UnixNano())

	// Parse command line arguments
	flag.Parse()

	if isHelp == true {
		printProgramUsage()
	}

	// Every command line argument is a player name
	players := flag.Args()

	if len(players) < 1 || len(players) > 4 {
		printProgramUsage()
	}

	for id, name := range players {
		fmt.Printf("Welcome, %s! (player %d)\n", name, id+1)
	}

	b := board.NewBoard(players)
	idToName := b.GetPlayersNames()

	// The main loop. Every iteration of the loop is a turn.
TurnLoop:
	for turnNumber := 1; ; turnNumber++ {
		id := b.GetCurrentPlayerID()
		fmt.Printf("Turn #%d for %s\n", turnNumber, idToName[id])

		// Obtain progress
		progress, _ := b.GetPlayersProgress()
		fmt.Println("Progress so far:")
		fmt.Print(progress[id])
		fmt.Println("Available tracks:")
		fmt.Println(b.GetCurrentPlayer().GetAvailableTracks(b.GetCompletedTracks()))

		// The roll-and-assign loop.
		// The player may decide at any time to stop rolling and cash in the turn progress (delta)
		for {
			ok := b.CurrentPlayerRoll()
			b.PrintDiceCollection()

			// Only proceed if there is a valid way to group the dice to hit an available track
			if !ok {
				fmt.Println("No combinations are possible! You lose your turn.")
				continue TurnLoop
			}

			dice := b.GetCurrentDice()

			// Validate that user's input denotes one of the possible combinations
			for {
				diceGroupings := parseGroupings()

				if len(diceGroupings) == 0 {
					fmt.Printf("Unable to parse dice selection. Try again.\n")
					continue
				}

				appliedDice := b.CurrentPlayerApplyDiceGroups(diceGroupings)

				if len(appliedDice) != 0 {
					break
				}

				fmt.Printf("Unable to apply dice: %+v, try again.\n")
			}

			b.PrintDiceCollection()

			_, delta := b.GetPlayersProgress()
			fmt.Println("Progress this turn:")
			fmt.Print(delta[id])
			fmt.Println("Available tracks:")
			fmt.Println(b.GetCurrentPlayer().GetAvailableTracks(b.GetCompletedTracks()))

			fmt.Println("Keep pressing your luck? \"yes\" or \"no\" (without quotes): ")
			var ans string
			fmt.Scanf("%s\n", &ans)

			switch ans {
			case "yes":
				continue
			case "no":
				b.GetCurrentPlayer().ApplyTurnDeltaToProgress()
				if len(b.GetCurrentPlayer().GetCompletedTracks()) >= 3 {
					fmt.Printf("On turn %d, %s wins! Congratulations!", turnNumber, idToName[id])
					os.Exit(0)
				}
				continue TurnLoop
			}
		}

	}

}

func parseGroupings() [][]int {
	var g1d1, g1d2, g2d1, g2d2 int
	fmt.Println("In the form of _ _ | _ _ (replace underscores with dice number, not value)...")
	fmt.Print("Enter dice groupings: ")
	fmt.Scanf("%d %d | %d %d\n", &g1d1, &g1d2, &g2d1, &g2d2)
	// If any of these variables are its zero-value, we weren't able to parse the value provided
	if g1d1 == 0 || g1d2 == 0 || g2d1 == 0 || g2d2 == 0 {
		return nil
	}

	diceGroupings := [][]int{[]int{g1d1 - 1, g1d2 - 1}, []int{g2d1 - 1, g2d2 - 1}}
	return diceGroupings
}

func printProgramUsage() {
	fmt.Println("You must provide between 1 and 4 player names.")
	os.Exit(0)
}
