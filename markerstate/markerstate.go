package markerstate

import (
	"bitbucket.org/deniscormier/cant-stop/stringutil"

	"fmt"
)

var MarkerStateTracks = []string{"2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"}

var ZeroMarkerState = MarkerState{
	"2":  0,
	"3":  0,
	"4":  0,
	"5":  0,
	"6":  0,
	"7":  0,
	"8":  0,
	"9":  0,
	"10": 0,
	"11": 0,
	"12": 0,
}

var AllTracksCompletedMarkerState = MarkerState{
	"2":  3,
	"3":  5,
	"4":  7,
	"5":  9,
	"6":  11,
	"7":  13,
	"8":  11,
	"9":  9,
	"10": 7,
	"11": 5,
	"12": 3,
}

type MarkerState map[string]int

func NewMarkerState() MarkerState {
	ms := make(MarkerState)
	for key, value := range ZeroMarkerState {
		ms[key] = value
	}

	return ms
}

func (ms MarkerState) String() string {
	str := ""

	for _, key := range MarkerStateTracks {
		str += fmt.Sprintf("%-3s", key)
	}

	str += "\n"

	for _, key := range MarkerStateTracks {
		str += fmt.Sprintf("%-3d", ms[key])
	}

	str += "\n"

	return str
}

// Are all tracks at or below their maximum value?
// Of course, tracks cannot be below 0
func (ms MarkerState) IsValid() bool {
	for key, maxValue := range AllTracksCompletedMarkerState {
		msValue := ms[key]
		if msValue < 0 || msValue > maxValue {
			return false
		}
	}

	return true
}

func (ms MarkerState) AddDelta(msDelta MarkerState) {
	for i := 0; i < len(MarkerStateTracks); i++ {
		key := MarkerStateTracks[i]
		ms[key] += msDelta[key]
	}
}

func (ms MarkerState) GetCompletedTracks() stringutil.Set {
	completedTracks := stringutil.Set{}

	for _, trackKey := range MarkerStateTracks {
		if ms[trackKey] == AllTracksCompletedMarkerState[trackKey] {
			completedTracks.Add(trackKey)
		}
	}

	return completedTracks
}

func (ms MarkerState) Reset() {
	for key, value := range ZeroMarkerState {
		ms[key] = value
	}
}
