package markerstate_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"testing"
)

func TestMarkerstate(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Markerstate Suite")
}
