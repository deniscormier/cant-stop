package markerstate_test

import (
	"bitbucket.org/deniscormier/cant-stop/markerstate"
	"bitbucket.org/deniscormier/cant-stop/stringutil"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Markerstate", func() {

	var markerState markerstate.MarkerState

	BeforeEach(func() {
		markerState = markerstate.NewMarkerState()
	})

	Context("A new MarkerState", func() {

		It("Should contain zeroes on each track", func() {
			Expect(markerState).To(Equal(markerstate.ZeroMarkerState))
		})

		It("Should contain no completed tracks", func() {
			Expect(markerState.GetCompletedTracks()).To(Equal(stringutil.NewStringSet([]string{})))
		})

	})

	Context("Two MarkerState put together", func() {

		BeforeEach(func() {
			markerState["2"] = 1
			markerState["4"] = 3

			markerStateDelta := markerstate.NewMarkerState()
			markerStateDelta["3"] = 2
			markerStateDelta["4"] = 4

			markerState.AddDelta(markerStateDelta)
		})

		It("Should have merged (added) the results together", func() {
			Expect(markerState["2"]).To(Equal(1))
			Expect(markerState["3"]).To(Equal(2))
			Expect(markerState["4"]).To(Equal(7))
			Expect(markerState["5"]).To(Equal(0))
			Expect(markerState["6"]).To(Equal(0))
			Expect(markerState["7"]).To(Equal(0))
			Expect(markerState["8"]).To(Equal(0))
			Expect(markerState["9"]).To(Equal(0))
			Expect(markerState["10"]).To(Equal(0))
			Expect(markerState["11"]).To(Equal(0))
			Expect(markerState["12"]).To(Equal(0))
		})

		It("Should contain one completed track: \"4\"", func() {
			Expect(markerState.GetCompletedTracks()).To(Equal(stringutil.NewStringSet([]string{"4"})))
		})

		It("Should contain zeroes on each track after resetting the MarkerState", func() {
			markerState.Reset()
			Expect(markerState).To(Equal(markerstate.ZeroMarkerState))
		})

		It("Should confirm that marker state is valid", func() {
			Expect(markerState.IsValid()).To(BeTrue())
		})

	})

	Context("A MarkerState with a value too large", func() {

		BeforeEach(func() {
			markerState["4"] = 8
		})

		It("Should confirm that marker state is invalid", func() {
			Expect(markerState.IsValid()).To(BeFalse())
		})

	})

})
