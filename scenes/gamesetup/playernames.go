package gamesetup

import (
	"github.com/oakmound/oak"
	"github.com/oakmound/oak/dlog"
	"github.com/oakmound/oak/render"

	"fmt"
	"strings"
)

var (
	padChar    = "_"
	nameMaxLen = 12
)

type PlayerNames struct {
	playerNameText       []*playerNameText
	selectedPlayerCursor *render.Sprite
	cursorPositions      []struct {
		x float64
		y float64
	}
	cursorPosIndex int
}

func NewPlayerNames(
	basePosX,
	basePosY float64,
	numPlayers int,
	font *render.Font,
	cursorImageFilepath string) *PlayerNames {

	pns := new(PlayerNames)

	pns.cursorPositions = []struct {
		x float64
		y float64
	}{
		{x: basePosX, y: basePosY},       // Player 1
		{x: basePosX, y: basePosY + 50},  // Player 2
		{x: basePosX, y: basePosY + 100}, // Player 3
		{x: basePosX, y: basePosY + 150}, // Player 4
		{x: basePosX, y: basePosY + 200}, // Start Game
	}

	pns.playerNameText = make([]*playerNameText, numPlayers)
	for i := 0; i < numPlayers; i++ {
		cursorPos := pns.cursorPositions[i]
		namePosX := cursorPos.x + 70
		namePosY := cursorPos.y + 30

		pns.playerNameText[i] = newPlayerNameText(
			font.NewStrText("", namePosX, namePosY),
			fmt.Sprintf("Player %d: ", i+1))
	}

	pns.selectedPlayerCursor = render.LoadSprite(cursorImageFilepath)
	pns.selectedPlayerCursor.SetPos(basePosX, basePosY)
	render.Draw(pns.selectedPlayerCursor, 0)

	render.Draw(font.NewStrText("Enter player names below", basePosX+50, basePosY-50), 0)

	pns.cursorPosIndex = 0

	return pns
}

// Pass writeable characters to whatever name the selection indicator is pointing to at that moment
func (pns *PlayerNames) KeyDown(_ int, eventKey interface{}) int {
	dlog.Verb(fmt.Sprintf("%T -> %+v", eventKey, eventKey))

	key := eventKey.(string)
	playerIndex := pns.cursorPosIndex

	// Check that our cursor points to a player name slot
	if playerIndex < 4 {

		// There's a good chance we have a writeable character if we only allow event keys of length one
		// Else, try to handle some other common keys
		if len(key) == 1 {
			if !oak.IsDown("LeftShift") {
				key = strings.ToLower(key)
			}
			pns.playerNameText[playerIndex].addChar(key)
		} else {
			if key == "Spacebar" {
				pns.playerNameText[playerIndex].addChar(" ")
			} else if key == "DeleteBackspace" {
				// Remove last character from whatever name the selection indicator is pointing to at that moment
				pns.playerNameText[playerIndex].backspace()
			}
		}

	}
	return 0
}

// Move selection indicator up one name
func (pns *PlayerNames) KeyDownUpArrow(_ int, _ interface{}) int {
	if pns.cursorPosIndex > 0 {
		pns.cursorPosIndex -= 1
		pns.setNewCursorPos()
	}
	return 0
}

// Move selection indicator down one name
func (pns *PlayerNames) KeyDownDownArrow(_ int, _ interface{}) int {
	if pns.cursorPosIndex < 4 {
		pns.cursorPosIndex += 1
		pns.setNewCursorPos()
	}
	return 0
}

// Move selection indicator down one name
func (pns *PlayerNames) IsReadyToStartGame() bool {
	// Given that the cursor is at "Start Game"...
	if pns.cursorPosIndex == len(pns.playerNameText) {

		// Look for a player name that's non-empty
		for _, playerNameText := range pns.playerNameText {

			// If one of the player names is non-empty, we can make use of player names
			if playerNameText.getPlayerName() != "" {
				return true
			}
		}

	}
	return false
}

// Return set of names
func (pns *PlayerNames) GetPlayerNames() []string {
	names := []string{}
	for _, pnt := range pns.playerNameText {
		name := pnt.getPlayerName()
		if name != "" {
			names = append(names, name)
		}
	}
	return names
}

func (pns *PlayerNames) setNewCursorPos() {
	newPos := pns.cursorPositions[pns.cursorPosIndex]
	pns.selectedPlayerCursor.SetPos(newPos.x, newPos.y)
}

type playerNameText struct {
	prefix     string
	playerName string
	renderText *render.Text
}

func newPlayerNameText(rText *render.Text, playerNamePrefix string) *playerNameText {
	pnt := new(playerNameText)
	pnt.prefix = playerNamePrefix
	pnt.playerName = ""
	pnt.renderText = rText
	render.Draw(rText, 0)
	pnt.update()
	return pnt
}

func (pnt *playerNameText) getPlayerName() string {
	return pnt.playerName
}

func (pnt *playerNameText) addChar(char string) {
	if len(pnt.playerName) < nameMaxLen {
		pnt.playerName += char
		pnt.update()
	}
}

func (pnt *playerNameText) backspace() {
	name := pnt.playerName
	if len(name) > 0 {
		pnt.playerName = name[0 : len(name)-1]
		pnt.update()
	}
}

func (pnt *playerNameText) update() {
	str := pnt.prefix + pnt.playerName
	for i := 0; i < nameMaxLen-len(pnt.playerName); i++ {
		str += padChar
	}
	pnt.renderText.SetString(str)
}
