package scenes

import (
	"bitbucket.org/deniscormier/cant-stop/board"
	"bitbucket.org/deniscormier/cant-stop/scenes/gamesetup"
	"bitbucket.org/deniscormier/cant-stop/scenes/play"

	"github.com/oakmound/oak"
	"github.com/oakmound/oak/dlog"
	"github.com/oakmound/oak/entities"
	"github.com/oakmound/oak/event"
	"github.com/oakmound/oak/render"

	"fmt"
	"image"
	"image/color"
	"math"
	"os"
	"path/filepath"
	"time"
)

var (
	SceneMenu      = "menu"
	SceneGameSetup = "player-names"
	ScenePlay      = "play"

	goToNextScene = false
	nextSceneName = ""

	programStart = time.Now()

	cantStopGame  *board.Board
	playerNames   *gamesetup.PlayerNames
	boardProgress *play.BoardProgress
	diceGroups    *play.DiceGroups
	diceRolled    *play.DiceRolled
	rollOrStop    *play.RollOrStop

	fullImageName = map[string]string{
		"enterKey":        filepath.Join("xelu_keyboard_prompts", "Keyboard_Black_Enter.png"),
		"escKey":          filepath.Join("xelu_keyboard_prompts", "Keyboard_Black_Esc.png"),
		"upArrow":         filepath.Join("xelu_keyboard_prompts", "Keyboard_Black_Arrow_Up.png"),
		"downArrow":       filepath.Join("xelu_keyboard_prompts", "Keyboard_Black_Arrow_Down.png"),
		"oneKey":          filepath.Join("xelu_keyboard_prompts", "Keyboard_Black_1_small.png"),
		"twoKey":          filepath.Join("xelu_keyboard_prompts", "Keyboard_Black_2_small.png"),
		"selectionCursor": filepath.Join("xelu_keyboard_prompts", "Directional_Arrow_Straight.png"),
		"diceOne":         filepath.Join("skoll", "dice-1.png"),
		"diceTwo":         filepath.Join("skoll", "dice-2.png"),
		"diceThree":       filepath.Join("skoll", "dice-3.png"),
		"diceFour":        filepath.Join("skoll", "dice-4.png"),
		"diceFive":        filepath.Join("skoll", "dice-5.png"),
		"diceSix":         filepath.Join("skoll", "dice-6.png"),
		"diceAbsent":      filepath.Join("deniscormier", "dice-absent.png"),
	}
	diceToImageName = map[int]string{
		1: "diceOne",
		2: "diceTwo",
		3: "diceThree",
		4: "diceFour",
		5: "diceFive",
		6: "diceSix",
	}

	colors = map[string]color.RGBA{
		"white":  color.RGBA{255, 255, 255, 255},
		"red":    color.RGBA{255, 0, 0, 255},
		"green":  color.RGBA{0, 255, 0, 255},
		"blue":   color.RGBA{0, 0, 255, 255},
		"yellow": color.RGBA{0, 255, 255, 255},

		"darkRed":    color.RGBA{100, 0, 0, 255},
		"darkGreen":  color.RGBA{0, 100, 0, 255},
		"darkBlue":   color.RGBA{0, 0, 100, 255},
		"darkYellow": color.RGBA{0, 100, 100, 255},
	}

	fullFontName = map[string]string{
		"kalam":   filepath.Join("Kalam", "Kalam-Regular.ttf"),
		"fredoka": filepath.Join("Fredoka_One", "FredokaOne-Regular.ttf"),
	}

	// Fully initialized in menu scene
	fonts = map[string]*render.Font{}
)

////////////////
// Menu scene //
////////////////

func MenuInit(prevScene string, inData interface{}) {
	/////////////////////////
	// Font initialization //
	/////////////////////////
	fontParameters := render.FontGenerator{
		File:    fullFontName["fredoka"],
		Color:   image.NewUniform(colors["white"]),
		Size:    200,
		Hinting: "",
		DPI:     10,
	}
	fonts["fredoka"] = fontParameters.Generate()

	fontParameters.File = fullFontName["kalam"]
	fonts["kalam20White"] = fontParameters.Generate()
	fontParameters.Color = image.NewUniform(colors["red"])
	fonts["kalam20Red"] = fontParameters.Generate()
	fontParameters.Color = image.NewUniform(colors["blue"])
	fonts["kalam20Blue"] = fontParameters.Generate()
	fontParameters.Color = image.NewUniform(colors["green"])
	fonts["kalam20Green"] = fontParameters.Generate()
	fontParameters.Color = image.NewUniform(colors["yellow"])
	fonts["kalam20Yellow"] = fontParameters.Generate()

	//////////
	// Draw //
	//////////
	render.Draw(fonts["kalam20White"].NewStrText("Can't Stop: a dice game by Sid Sackson", 120, 50), 0)

	diceOne := entities.NewMoving(328, 180, 64, 64, render.LoadSprite(fullImageName["diceOne"]), 0, 0)
	render.Draw(diceOne.R, 0)
	diceOne.Bind(traceCircle(328, 180, 4*math.Pi/3, 100), "EnterFrame")

	diceTwo := entities.NewMoving(328, 180, 64, 64, render.LoadSprite(fullImageName["diceTwo"]), 0, 0)
	render.Draw(diceTwo.R, 0)
	diceTwo.Bind(traceCircle(328, 180, math.Pi, 100), "EnterFrame")

	diceThree := entities.NewMoving(328, 180, 64, 64, render.LoadSprite(fullImageName["diceThree"]), 0, 0)
	render.Draw(diceThree.R, 0)
	diceThree.Bind(traceCircle(328, 180, 2*math.Pi/3, 100), "EnterFrame")

	diceFour := entities.NewMoving(328, 180, 64, 64, render.LoadSprite(fullImageName["diceFour"]), 0, 0)
	render.Draw(diceFour.R, 0)
	diceFour.Bind(traceCircle(328, 180, math.Pi/3, 100), "EnterFrame")

	diceFive := entities.NewMoving(328, 180, 64, 64, render.LoadSprite(fullImageName["diceFive"]), 0, 0)
	render.Draw(diceFive.R, 0)
	diceFive.Bind(traceCircle(328, 180, 0, 100), "EnterFrame")

	diceSix := entities.NewMoving(328, 180, 64, 64, render.LoadSprite(fullImageName["diceSix"]), 0, 0)
	render.Draw(diceSix.R, 0)
	diceSix.Bind(traceCircle(328, 180, 5*math.Pi/3, 100), "EnterFrame")

	// Proceed to setup screen
	render.Draw(fonts["kalam20White"].NewStrText("Setup game...", 80, 330), 0)
	enterKey := render.LoadSprite(fullImageName["enterKey"])
	enterKey.SetPos(100, 340)
	render.Draw(enterKey, 0)

	// Close the app
	render.Draw(fonts["kalam20White"].NewStrText("Quit", 540, 330), 0)
	escKey := render.LoadSprite(fullImageName["escKey"])
	escKey.SetPos(520, 340)
	render.Draw(escKey, 0)

	/////////////////
	// Bind events //
	/////////////////
	event.GlobalBind(func(int, interface{}) int {
		goToNextScene = true
		nextSceneName = SceneGameSetup
		return 0
	}, "KeyDownReturnEnter")

	event.GlobalBind(func(int, interface{}) int {
		os.Exit(0)
		return 0
	}, "KeyDownEscape")
}

func MenuLoop() bool {
	if goToNextScene == true {
		goToNextScene = false
		return false
	}
	return true
}

func MenuEnd() (string, *oak.SceneResult) {
	return nextSceneName, &oak.SceneResult{
		NextSceneInput: nil,
		Transition:     oak.TransitionFade(0.05, 60),
	}
}

//////////////////////////////////////////////
// Bind events for moving dice in main menu //
//////////////////////////////////////////////

func traceCircle(xCenter, yCenter, radius, circumferenceOffset float64) func(int, interface{}) int {
	return func(id int, nothing interface{}) int {
		sinceProgramStart := time.Since(programStart)
		p := event.GetEntity(id).(*entities.Moving)

		// Both between -circumferenceOffset and circumferenceOffset
		// xOff and yOff are set to trace a circle in a clockwise motion
		xOff := math.Cos(float64(sinceProgramStart)/float64(time.Second)+radius) * circumferenceOffset
		yOff := math.Sin(float64(sinceProgramStart)/float64(time.Second)+radius) * circumferenceOffset

		p.SetPos(xCenter+xOff, yCenter+yOff)
		return 0
	}
}

////////////////////////
// Player Names scene //
////////////////////////

func GameSetupInit(prevScene string, inData interface{}) {
	//////////
	// Draw //
	//////////
	playerNames = gamesetup.NewPlayerNames(150, 100, 4, fonts["kalam20White"], fullImageName["selectionCursor"])

	upArrow := render.LoadSprite(fullImageName["upArrow"])
	upArrow.SetPos(20, 90)
	render.Draw(upArrow, 0)

	downArrow := render.LoadSprite(fullImageName["downArrow"])
	downArrow.SetPos(20, 200)
	render.Draw(downArrow, 0)

	render.Draw(fonts["kalam20White"].NewStrText("Start game", 220, 330), 0)
	enterKey := render.LoadSprite(fullImageName["enterKey"])
	enterKey.SetPos(230, 340)
	render.Draw(enterKey, 0)

	render.Draw(fonts["kalam20White"].NewStrText("Main menu", 510, 330), 0)
	escKey := render.LoadSprite(fullImageName["escKey"])
	escKey.SetPos(520, 340)
	render.Draw(escKey, 0)

	/////////////////
	// Bind events //
	/////////////////

	// Most keypresses will be handled internally by the PlayerNames objects
	event.GlobalBind(playerNames.KeyDown, "KeyDown")
	event.GlobalBind(playerNames.KeyDownUpArrow, "KeyDownUpArrow")
	event.GlobalBind(playerNames.KeyDownDownArrow, "KeyDownDownArrow")

	// If selection indicator on "Start game", begin game with the number of players having a non-empty name
	// Else, hand off keypress to PlayerNames object
	event.GlobalBind(func(_ int, _ interface{}) int {
		if playerNames.IsReadyToStartGame() {
			goToNextScene = true
			nextSceneName = ScenePlay
			return 0
		}
		return playerNames.KeyDownDownArrow(0, struct{}{})
	}, "KeyDownReturnEnter")

	// Return to main menu
	event.GlobalBind(func(_ int, _ interface{}) int {
		goToNextScene = true
		nextSceneName = SceneMenu
		return 0
	}, "KeyDownEscape")
}

func GameSetupLoop() bool {
	if goToNextScene == true {
		goToNextScene = false
		return false
	}
	return true
}

func GameSetupEnd() (string, *oak.SceneResult) {
	// Pass player names to Play scene
	return nextSceneName, &oak.SceneResult{
		NextSceneInput: playerNames.GetPlayerNames(),
		Transition:     oak.TransitionFade(0.05, 60),
	}
}

////////////////
// Play scene //
////////////////

func PlayInit(prevScene string, inData interface{}) {
	cantStopGame = board.NewBoard(inData.([]string))

	//////////
	// Draw //
	//////////

	// Render lines for screen border
	render.Draw(render.NewThickLine(0, 0, 0, 720, colors["white"], 8), 0)
	render.Draw(render.NewThickLine(0, 0, 1080, 0, colors["white"], 8), 0)
	render.Draw(render.NewThickLine(0, 720, 1080, 720, colors["white"], 8), 0)
	render.Draw(render.NewThickLine(1080, 0, 1080, 720, colors["white"], 8), 0)

	// Cross-lines to form 4 panels
	render.Draw(render.NewThickLine(860, 0, 860, 720, colors["white"], 4), 0)
	render.Draw(render.NewThickLine(0, 500, 1080, 500, colors["white"], 4), 0)

	//render.Draw(fonts["kalam20White"].NewStrText("Turn #1", 40, 50), 0)
	//render.Draw(fonts["kalam20White"].NewStrText(fmt.Sprintf("Players: %+v", cantStopGame.GetPlayersNames()), 80, 50), 0)

	// prepare map for colored names
	coloredPlayerNames := []*render.Text{}
	playerNameFonts := []*render.Font{fonts["kalam20Red"], fonts["kalam20Green"], fonts["kalam20Blue"], fonts["kalam20Yellow"]}

	playerNames := cantStopGame.GetPlayersNames()
	for id, _ := range playerNames {
		coloredPlayerNames = append(coloredPlayerNames, playerNameFonts[id].NewStrText(playerNames[id], 660, 90))
	}

	render.Draw(fonts["kalam20White"].NewStrText("Active player", 660, 50), 0)
	render.Draw(render.NewThickLine(660, 60, 810, 60, colors["white"], 2), 0)

	// First player name
	render.Draw(coloredPlayerNames[0], 0)

	// Draw board progress and markers
	playerColors := []color.RGBA{colors["red"], colors["green"], colors["blue"], colors["yellow"]}
	playerDarkColors := []color.RGBA{colors["darkRed"], colors["darkGreen"], colors["darkBlue"], colors["darkYellow"]}

	boardProgress := play.NewBoardProgress(60, 444, cantStopGame, fonts["fredoka"], colors["white"], playerColors, playerDarkColors)

	// Draw selected groups for rolled dice
	diceGroups = play.NewDiceGroups(48, 596, fonts["kalam20White"])

	// Kick off first roll (should always return true)
	ok := cantStopGame.CurrentPlayerRoll()
	if !ok {
		dlog.Error("First roll returned false.")
		os.Exit(1)
	}

	// Draw rolled dice
	diceRolled = play.NewDiceRolled(876, 40, cantStopGame, diceGroups, 4, fonts["kalam20White"])
	diceRolled.Roll()

	rollOrStop := play.NewRollOrStop(600, 520, cantStopGame, fonts["kalam20White"])

	// Prompt for exiting the game
	// TODO make this go to an options menu instead
	render.Draw(fonts["kalam20White"].NewStrText("Main menu", 906, 566), 0)
	escKey := render.LoadSprite(fullImageName["escKey"])
	escKey.SetPos(918, 574)
	render.Draw(escKey, 0)

	/////////////////
	// Bind events //
	/////////////////

	// Temporary: Help find coordinates for placing objects
	event.GlobalBind(func(_ int, eventInfo interface{}) int {
		dlog.Verb(fmt.Sprintf("%T -> %+v", eventInfo, eventInfo))
		return 0
	}, "MouseRelease")

	// Keypresses 1 and 2 will be handled internally by the DiceRolled object to
	// select dice groupings
	event.GlobalBind(diceRolled.KeyDown1, "KeyDown1")
	event.GlobalBind(diceRolled.KeyDown2, "KeyDown2")

	// Keypresses 1 and 2 also represent choosing between "roll again" and "stop (cash in)"
	event.GlobalBind(rollOrStop.KeyDownUpArrow, "KeyDownUpArrow")
	event.GlobalBind(rollOrStop.KeyDownDownArrow, "KeyDownDownArrow")

	changePlayers := false
	event.GlobalBind(func(_ int, _ interface{}) int {
		if diceRolled.IsWaitingForRoll() {
			if changePlayers {
				changePlayers = false
				playerID := cantStopGame.GetCurrentPlayer().GetID()
				coloredPlayerNames[playerID].UnDraw()

				cantStopGame.NextPlayer()

				playerID = cantStopGame.GetCurrentPlayer().GetID()
				render.Draw(coloredPlayerNames[playerID], 0)
			}
			ok := diceRolled.Roll()
			if !ok {
				changePlayers = true
			}
		} else if diceRolled.IsActive() {
			diceIndices := diceRolled.GetGroupDiceIndices()
			if diceIndices != nil {
				// We have 4 dice selected into 2 groups
				sumsApplied := cantStopGame.CurrentPlayerApplyDiceGroups(diceIndices)
				if sumsApplied != nil {
					// The group configuration resulted in at least 1 track updated
					diceRolled.Deactivate(false)
					rollOrStop.Activate()
				}
			}
		} else if rollOrStop.IsActive() {
			action := rollOrStop.SelectAction()
			if action == 0 { // roll
				diceRolled.WaitForUserAcknowlegement("        Roll again!")
				rollOrStop.Deactivate()
			} else if action == 1 { // stop
				changePlayers = true
				cantStopGame.CurrentPlayerCashIn()
				if len(cantStopGame.GetCurrentPlayer().GetCompletedTracks()) >= 3 {
					render.Draw(fonts["kalam20White"].NewStrText("Congratulations "+cantStopGame.GetCurrentPlayer().GetName(), 500, 550), 0)
					render.Draw(fonts["kalam20White"].NewStrText("You win!", 500, 590), 0)
					rollOrStop.Deactivate()
					boardProgress.Refresh()
					return 0
				}

				diceRolled.WaitForUserAcknowlegement("Results are cashed in.")
				rollOrStop.Deactivate()
			}
		}
		boardProgress.Refresh()
		return 0
	}, "KeyDownReturnEnter")

	// R to reset dice group selection
	// TODO: Backspace to revert group selection for the last die
	event.GlobalBind(diceRolled.ResetGroupSelection, "KeyDownR")

	// Return to main menu
	event.GlobalBind(func(_ int, _ interface{}) int {
		goToNextScene = true
		nextSceneName = SceneMenu
		return 0
	}, "KeyDownEscape")
}

func PlayLoop() bool {
	if goToNextScene == true {
		goToNextScene = false
		return false
	}
	return true
}

func PlayEnd() (string, *oak.SceneResult) {
	return nextSceneName, &oak.SceneResult{
		NextSceneInput: nil,
		Transition:     oak.TransitionFade(0.05, 60),
	}
}
