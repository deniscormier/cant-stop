package play

import (
	"github.com/oakmound/oak/dlog"
	"github.com/oakmound/oak/render"

	"fmt"
	"image"
)

type DiceGroups struct {
	absentDiceRGBA  *image.RGBA
	diceValueToRGBA map[int]*image.RGBA
	diceSprites     [][]*render.Sprite
	diceGroupValues [][]int
	groupSumText    []*render.Text
}

func NewDiceGroups(basePosX, basePosY float64, font *render.Font) *DiceGroups {
	dg := new(DiceGroups)

	// Initializing RGBAs for setting sprite graphics with later
	dg.absentDiceRGBA = render.LoadSprite(fullImageName["diceAbsent"]).GetRGBA()
	dg.diceValueToRGBA = map[int]*image.RGBA{
		1: render.LoadSprite(fullImageName["diceOne"]).GetRGBA(),
		2: render.LoadSprite(fullImageName["diceTwo"]).GetRGBA(),
		3: render.LoadSprite(fullImageName["diceThree"]).GetRGBA(),
		4: render.LoadSprite(fullImageName["diceFour"]).GetRGBA(),
		5: render.LoadSprite(fullImageName["diceFive"]).GetRGBA(),
		6: render.LoadSprite(fullImageName["diceSix"]).GetRGBA(),
	}

	// Initialize dice slots with the "absent" graphic
	dg.diceSprites = [][]*render.Sprite{
		[]*render.Sprite{
			render.LoadSprite(fullImageName["diceAbsent"]),
			render.LoadSprite(fullImageName["diceAbsent"]),
		},
		[]*render.Sprite{
			render.LoadSprite(fullImageName["diceAbsent"]),
			render.LoadSprite(fullImageName["diceAbsent"]),
		},
	}
	dg.diceGroupValues = [][]int{
		[]int{
			0,
			0,
		},
		[]int{
			0,
			0,
		},
	}

	dg.diceSprites[0][0].SetPos(basePosX, basePosY)
	dg.diceSprites[0][1].SetPos(basePosX+64, basePosY)
	dg.diceSprites[1][0].SetPos(basePosX+280, basePosY)
	dg.diceSprites[1][1].SetPos(basePosX+344, basePosY)

	render.Draw(dg.diceSprites[0][0], 0)
	render.Draw(dg.diceSprites[0][1], 0)
	render.Draw(dg.diceSprites[1][0], 0)
	render.Draw(dg.diceSprites[1][1], 0)

	// Draw selected groups for rolled dice
	render.Draw(font.NewStrText("Group 1", basePosX+14, basePosY-18), 0)
	render.Draw(font.NewStrText("Group 2", basePosX+294, basePosY-18), 0)

	dg.groupSumText = []*render.Text{
		font.NewStrText("= 0", basePosX+140, basePosY+44),
		font.NewStrText("= 0", basePosX+420, basePosY+44),
	}
	render.Draw(dg.groupSumText[0], 0)
	render.Draw(dg.groupSumText[1], 0)

	return dg
}

// Return false if the group is full. Return true otherwise.
func (dg *DiceGroups) AddDieToGroup(dieValue int, groupIndex int) bool {
	dlog.Verb(fmt.Sprintf("Setting die %d in group %d", dieValue, groupIndex))

	// Try filling either slot in the group.
	if dg.diceGroupValues[groupIndex][0] == 0 {
		dg.diceSprites[groupIndex][0].SetRGBA(dg.diceValueToRGBA[dieValue])
		dg.diceGroupValues[groupIndex][0] = dieValue

		sum := dg.diceGroupValues[groupIndex][0] + dg.diceGroupValues[groupIndex][1]
		dg.groupSumText[groupIndex].SetString(fmt.Sprintf("= %d", sum))
	} else if dg.diceGroupValues[groupIndex][1] == 0 {
		dg.diceSprites[groupIndex][1].SetRGBA(dg.diceValueToRGBA[dieValue])
		dg.diceGroupValues[groupIndex][1] = dieValue

		sum := dg.diceGroupValues[groupIndex][0] + dg.diceGroupValues[groupIndex][1]
		dg.groupSumText[groupIndex].SetString(fmt.Sprintf("= %d", sum))
	} else {
		// No room in the group for an additional die.
		return false
	}

	return true
}

func (dg *DiceGroups) ResetGroups() {
	dg.removeDieFromGroup(0, 0)
	dg.removeDieFromGroup(1, 0)
	dg.removeDieFromGroup(0, 1)
	dg.removeDieFromGroup(1, 1)
	dg.groupSumText[0].SetString("= 0")
	dg.groupSumText[1].SetString("= 0")
}

// Clear slot in group
func (dg *DiceGroups) removeDieFromGroup(dieIndex int, groupIndex int) {
	dlog.Verb(fmt.Sprintf("Removing die index %d in group %d", dieIndex, groupIndex))

	dg.diceSprites[groupIndex][dieIndex].SetRGBA(dg.absentDiceRGBA)
	dg.diceGroupValues[groupIndex][dieIndex] = 0
}
