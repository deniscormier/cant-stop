package play

import (
	"bitbucket.org/deniscormier/cant-stop/board"
	"github.com/oakmound/oak/dlog"
	"github.com/oakmound/oak/render"

	"fmt"
	"image"
)

type DiceRolled struct {
	game            *board.Board
	diceGroups      *DiceGroups
	absentDiceRGBA  *image.RGBA
	diceValueToRGBA map[int]*image.RGBA
	cursorPositions []struct {
		x float64
		y float64
	}
	cursorPosIndex    int
	diceSprites       []*render.Sprite
	diceValues        []int
	diceGroupIndices  [][]int
	selectedDieCursor *render.Sprite
	oneKey            *render.Sprite
	twoKey            *render.Sprite
	confirmText       *render.Text
	enterKey          *render.Sprite
	userAckText       *render.Text
	active            bool
	waitingForRoll    bool
}

func NewDiceRolled(basePosX, basePosY float64, game *board.Board, diceGroups *DiceGroups, numDice int, font *render.Font) *DiceRolled {
	dr := new(DiceRolled)

	dr.game = game
	dr.diceGroups = diceGroups

	// Initializing RGBAs for setting sprite graphics with later
	dr.absentDiceRGBA = render.LoadSprite(fullImageName["diceAbsent"]).GetRGBA()
	dr.diceValueToRGBA = map[int]*image.RGBA{
		0: dr.absentDiceRGBA,
		1: render.LoadSprite(fullImageName["diceOne"]).GetRGBA(),
		2: render.LoadSprite(fullImageName["diceTwo"]).GetRGBA(),
		3: render.LoadSprite(fullImageName["diceThree"]).GetRGBA(),
		4: render.LoadSprite(fullImageName["diceFour"]).GetRGBA(),
		5: render.LoadSprite(fullImageName["diceFive"]).GetRGBA(),
		6: render.LoadSprite(fullImageName["diceSix"]).GetRGBA(),
	}

	dr.cursorPositions = []struct {
		x float64
		y float64
	}{
		{x: basePosX, y: basePosY},       // Dice slot 1
		{x: basePosX, y: basePosY + 116}, // Dice slot 2
		{x: basePosX, y: basePosY + 232}, // Dice slot 3
		{x: basePosX, y: basePosY + 348}, // Dice slot 4
		{x: 600, y: 606},                 // Confirming group selection
	}
	dr.cursorPosIndex = 0

	// Initialize dice slots with the "absent" graphic
	dr.diceSprites = make([]*render.Sprite, 0, numDice)
	for i := 0; i < numDice; i++ {
		dr.diceSprites = append(dr.diceSprites, render.LoadSprite(fullImageName["diceAbsent"]))
		dr.diceSprites[i].SetPos(basePosX+60, (basePosY-10)+116*float64(i))
		render.Draw(dr.diceSprites[i], 0)
	}

	dr.diceGroupIndices = [][]int{
		[]int{},
		[]int{},
	}

	// Draw prompts around the first dice slot
	dr.selectedDieCursor = render.LoadSprite(fullImageName["selectionCursor"])
	dr.selectedDieCursor.SetPos(basePosX, basePosY)
	render.Draw(dr.selectedDieCursor, 0)

	dr.oneKey = render.LoadSprite(fullImageName["oneKey"])
	dr.oneKey.SetPos(basePosX+40, basePosY+50)
	render.Draw(dr.oneKey, 0)

	dr.twoKey = render.LoadSprite(fullImageName["twoKey"])
	dr.twoKey.SetPos(basePosX+94, basePosY+50)
	render.Draw(dr.twoKey, 0)

	// Prompt for confirming group selection
	dr.confirmText = font.NewStrText("Confirm", basePosX-206, basePosY+526)
	render.Draw(dr.confirmText, 0)

	dr.enterKey = render.LoadSprite(fullImageName["enterKey"])
	dr.enterKey.SetPos(basePosX-206, basePosY+536)
	render.Draw(dr.enterKey, 0)

	dr.userAckText = font.NewStrText("Default message", basePosX-306, basePosY+526)
	dr.userAckText.UnDraw()

	dr.active = false
	dr.waitingForRoll = true
	return dr
}

// Return false if we busted
func (dr *DiceRolled) Roll() bool {
	ok := dr.game.CurrentPlayerRoll()
	if ok {
		dr.userAckText.UnDraw()
		dr.Activate()
		dr.waitingForRoll = false
		return true
	} else {
		dr.Deactivate(true)
		dr.WaitForUserAcknowlegement("      You busted!")
		return false
	}
}

func (dr *DiceRolled) WaitForUserAcknowlegement(msg string) {
	dr.userAckText.SetString(msg)
	show(dr.userAckText)
	show(dr.enterKey)
	dr.waitingForRoll = true
}

func (dr *DiceRolled) Activate() {
	// We assume the dice have already been rolled
	dr.diceValues = dr.game.GetCurrentDice()
	dlog.Verb(fmt.Sprintf("Current dice: %+v", dr.diceValues))

	dr.active = true
	dr.ResetGroupSelection(0, struct{}{})
}

func (dr *DiceRolled) Deactivate(showRolledDice bool) int {
	if showRolledDice {
		dr.diceValues = dr.game.GetCurrentDice()
	} else {
		dr.diceValues = []int{0, 0, 0, 0}
	}

	dr.ResetGroupSelection(0, struct{}{})

	dr.oneKey.UnDraw()
	dr.twoKey.UnDraw()
	dr.selectedDieCursor.UnDraw()
	dr.confirmText.UnDraw()
	dr.enterKey.UnDraw()

	dr.active = false
	dr.waitingForRoll = false
	return 0
}

func (dr *DiceRolled) IsActive() bool {
	return dr.active
}

func (dr *DiceRolled) IsWaitingForRoll() bool {
	return dr.waitingForRoll
}

// Assign dice to group index 0 (group 1)
func (dr *DiceRolled) KeyDown1(_ int, _ interface{}) int {
	if dr.active && dr.cursorPosIndex < len(dr.diceValues) {
		ok := dr.diceGroups.AddDieToGroup(dr.diceValues[dr.cursorPosIndex], 0)
		if ok {
			dr.diceGroupIndices[0] = append(dr.diceGroupIndices[0], dr.cursorPosIndex)
			dr.diceSprites[dr.cursorPosIndex].SetRGBA(dr.absentDiceRGBA)
			dr.cursorPosIndex += 1
			dr.setNewCursorPos()
		}
	}
	return 0
}

// Assign dice to group index 1 (group 2)
func (dr *DiceRolled) KeyDown2(_ int, _ interface{}) int {
	if dr.active && dr.cursorPosIndex < len(dr.diceValues) {
		ok := dr.diceGroups.AddDieToGroup(dr.diceValues[dr.cursorPosIndex], 1)
		if ok {
			dr.diceGroupIndices[1] = append(dr.diceGroupIndices[1], dr.cursorPosIndex)
			dr.diceSprites[dr.cursorPosIndex].SetRGBA(dr.absentDiceRGBA)
			dr.cursorPosIndex += 1
			dr.setNewCursorPos()
		}
	}
	return 0
}

// After assigning dice groups, assign group sums to tracks
func (dr *DiceRolled) GetGroupDiceIndices() [][]int {
	if len(dr.diceGroupIndices[0]) == 2 && len(dr.diceGroupIndices[1]) == 2 {
		return dr.diceGroupIndices
	}
	return nil
}

func (dr *DiceRolled) ResetGroupSelection(_ int, _ interface{}) int {
	dr.diceGroups.ResetGroups()
	dr.resetDiceDisplay()
	dr.diceGroupIndices = [][]int{
		[]int{},
		[]int{},
	}
	return 0
}

func (dr *DiceRolled) resetDiceDisplay() {
	// Set dice slots to rolled dice
	for i, die := range dr.diceValues {
		dr.diceSprites[i].SetRGBA(dr.diceValueToRGBA[die])
	}

	// Show prompts (in case they were hidden previously)
	show(dr.selectedDieCursor)
	show(dr.oneKey)
	show(dr.twoKey)
	show(dr.confirmText)
	show(dr.enterKey)

	// Reposition prompts around the first dice
	dr.cursorPosIndex = 0
	dr.setNewCursorPos()
}

func (dr *DiceRolled) setNewCursorPos() {
	newPos := dr.cursorPositions[dr.cursorPosIndex]
	dr.selectedDieCursor.SetPos(newPos.x, newPos.y)

	if dr.cursorPosIndex == len(dr.cursorPositions)-1 {
		dr.oneKey.UnDraw()
		dr.twoKey.UnDraw()
	} else {
		dr.oneKey.SetPos(newPos.x+40, newPos.y+50)
		dr.twoKey.SetPos(newPos.x+94, newPos.y+50)
	}
}
