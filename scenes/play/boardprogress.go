package play

import (
	"bitbucket.org/deniscormier/cant-stop/board"
	"bitbucket.org/deniscormier/cant-stop/markerstate"
	"github.com/oakmound/oak/render"

	"image/color"
)

var (
	playerSquareOffset = []struct {
		x float64
		y float64
	}{
		{x: -6, y: -6}, // Player 1
		{x: 6, y: -6},  // Player 2
		{x: -6, y: 6},  // Player 3
		{x: 6, y: 6},   // Player 4
	}
)

type BoardProgress struct {
	basePosX        float64
	basePosY        float64
	game            *board.Board
	turnDeltaColors []color.RGBA

	// Array of maps... bp.progressSquares[0]["2"] means to get a progress sprite for player 0, track 2
	progressSquares  []map[string]*render.Sprite
	turnDeltaSquares []*render.Sprite
}

func NewBoardProgress(basePosX, basePosY float64, game *board.Board, font *render.Font, defaultSpriteColor color.RGBA,
	playerColors []color.RGBA, playerDarkColors []color.RGBA) *BoardProgress {

	bp := new(BoardProgress)

	bp.basePosX = basePosX
	bp.basePosY = basePosY
	bp.game = game
	bp.turnDeltaColors = playerDarkColors

	bp.progressSquares = []map[string]*render.Sprite{}
	for i := 0; i < len(game.GetPlayersNames()); i++ {
		bp.progressSquares = append(bp.progressSquares, map[string]*render.Sprite{})
	}
	bp.turnDeltaSquares = []*render.Sprite{}

	// '_' because there shouldn't be a need to track turn delta upon instantiation
	progress, _ := game.GetPlayersProgress()

	for playerID, _ := range game.GetPlayersNames() {
		for iInt, track := range markerstate.MarkerStateTracks {
			i := float64(iInt)
			trackValue := progress[playerID][track]
			trackMax := markerstate.AllTracksCompletedMarkerState[track]

			for jInt := 0; jInt <= trackMax; jInt++ {
				j := float64(jInt)
				render.Draw(font.NewStrText(track, basePosX+60*i, basePosY-30*j), 0)
				if trackValue == jInt {
					// Draw a small square that represents the player's marker for the track
					offX := playerSquareOffset[playerID].x
					offY := playerSquareOffset[playerID].y
					bp.progressSquares[playerID][track] = render.NewThickLine(
						basePosX-12+offX+60*i,
						basePosY-12+offY-30*j,
						basePosX-12+offX+60*i,
						basePosY-12+offY-30*j,
						playerColors[playerID], 4)
					render.Draw(bp.progressSquares[playerID][track], 0)
				}
			}

			// Draw a line to separate the final step on a track
			render.Draw(render.NewThickLine(
				basePosX-4+60*i,
				basePosY+4-30*float64(trackMax),
				basePosX+22+60*i,
				basePosY+4-30*float64(trackMax),
				defaultSpriteColor,
				1), 0)
		}
	}

	return bp
}

func (bp *BoardProgress) Refresh() {
	progress, turnDelta := bp.game.GetPlayersProgress()
	playerID := bp.game.GetCurrentPlayerID()

	for _, sprite := range bp.turnDeltaSquares {
		sprite.UnDraw()
	}
	bp.turnDeltaSquares = []*render.Sprite{}

	for iInt, track := range markerstate.MarkerStateTracks {
		i := float64(iInt)
		trackValue := progress[playerID][track]
		trackMax := markerstate.AllTracksCompletedMarkerState[track]

		for jInt := 0; jInt <= trackMax; jInt++ {
			j := float64(jInt)
			// Correct the position of progress squares, instantiate temporary delta squares
			if trackValue == jInt {
				offX := playerSquareOffset[playerID].x
				offY := playerSquareOffset[playerID].y
				bp.progressSquares[playerID][track].SetPos(bp.basePosX-16+offX+60*i, bp.basePosY-16+offY-30*j)

				trackDelta := turnDelta[playerID][track]
				if trackDelta != 0 {
					td := float64(trackDelta)
					sprite := render.NewThickLine(
						bp.basePosX-12+offX+60*i,
						bp.basePosY-12+offY-30*(j+td),
						bp.basePosX-12+offX+60*i,
						bp.basePosY-12+offY-30*(j+td),
						bp.turnDeltaColors[playerID], 4)
					render.Draw(sprite, 0)
					bp.turnDeltaSquares = append(bp.turnDeltaSquares, sprite)
				}
			}
		}
	}
}
