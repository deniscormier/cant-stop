package play

import (
	"github.com/oakmound/oak/render"

	"path/filepath"
)

var (
	fullImageName = map[string]string{
		"enterKey":        filepath.Join("xelu_keyboard_prompts", "Keyboard_Black_Enter.png"),
		"oneKey":          filepath.Join("xelu_keyboard_prompts", "Keyboard_Black_1_small.png"),
		"twoKey":          filepath.Join("xelu_keyboard_prompts", "Keyboard_Black_2_small.png"),
		"selectionCursor": filepath.Join("xelu_keyboard_prompts", "Directional_Arrow_Straight.png"),
		"diceOne":         filepath.Join("skoll", "dice-1.png"),
		"diceTwo":         filepath.Join("skoll", "dice-2.png"),
		"diceThree":       filepath.Join("skoll", "dice-3.png"),
		"diceFour":        filepath.Join("skoll", "dice-4.png"),
		"diceFive":        filepath.Join("skoll", "dice-5.png"),
		"diceSix":         filepath.Join("skoll", "dice-6.png"),
		"diceAbsent":      filepath.Join("deniscormier", "dice-absent.png"),
	}
)

// Determine if a sprite was hidden previously.
// If so, set its layer properly and put on draw stack again.
func show(s render.Renderable) {
	if s.GetLayer() != 0 {
		s.SetLayer(0)
		render.Draw(s, 0)
	}
}
