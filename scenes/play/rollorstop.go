package play

import (
	"bitbucket.org/deniscormier/cant-stop/board"
	"github.com/oakmound/oak/dlog"
	"github.com/oakmound/oak/render"
)

type RollOrStop struct {
	game            *board.Board
	cursorPositions []struct {
		x float64
		y float64
	}
	cursorPosIndex       int
	selectedOptionCursor *render.Sprite
	rollText             *render.Text
	stopText             *render.Text
	enterKey             *render.Sprite
	active               bool
}

func NewRollOrStop(basePosX, basePosY float64, game *board.Board, font *render.Font) *RollOrStop {
	rs := new(RollOrStop)

	rs.game = game

	rs.cursorPositions = []struct {
		x float64
		y float64
	}{
		{x: basePosX, y: basePosY},      // Roll again
		{x: basePosX, y: basePosY + 40}, // Stop (cash in)
	}
	rs.cursorPosIndex = 0

	// Draw prompt at default location (roll again)
	rs.selectedOptionCursor = render.LoadSprite(fullImageName["selectionCursor"])
	rs.selectedOptionCursor.SetPos(basePosX, basePosY)
	render.Draw(rs.selectedOptionCursor, 0)

	// Text for roll again
	rs.rollText = font.NewStrText("Roll again", basePosX+62, basePosY+30)
	render.Draw(rs.rollText, 0)

	// Text for stop
	rs.stopText = font.NewStrText("Stop (cash in)", basePosX+62, basePosY+70)
	render.Draw(rs.stopText, 0)

	rs.enterKey = render.LoadSprite(fullImageName["enterKey"])
	rs.enterKey.SetPos(basePosX+70, basePosY+80)
	render.Draw(rs.enterKey, 0)

	rs.Deactivate()
	return rs
}

func (rs *RollOrStop) KeyDownUpArrow(_ int, _ interface{}) int {
	if rs.active {
		rs.cursorPosIndex = 0
		rs.setNewCursorPos()
	}
	return 0
}

func (rs *RollOrStop) KeyDownDownArrow(_ int, _ interface{}) int {
	if rs.active {
		rs.cursorPosIndex = 1
		rs.setNewCursorPos()
	}
	return 0
}

// Return the index of the cursor
func (rs *RollOrStop) SelectAction() int {
	if rs.active {
		if rs.cursorPosIndex == 0 {
			dlog.Verb("Selected option: Roll again")
			return 0
		}
		if rs.cursorPosIndex == 1 {
			dlog.Verb("Selected option: Stop (cash in)")
			return 1
		}
	}
	return -1
}

func (rs *RollOrStop) IsActive() bool {
	return rs.active
}

func (rs *RollOrStop) Deactivate() {
	rs.selectedOptionCursor.UnDraw()
	rs.rollText.UnDraw()
	rs.stopText.UnDraw()
	rs.enterKey.UnDraw()

	rs.active = false
}

func (rs *RollOrStop) Activate() {
	rs.active = true

	// Show prompts (in case they were hidden previously)
	show(rs.selectedOptionCursor)
	show(rs.rollText)
	show(rs.stopText)
	show(rs.enterKey)

	// Reposition cursor on first option
	rs.cursorPosIndex = 0
	rs.setNewCursorPos()
}

func (rs *RollOrStop) setNewCursorPos() {
	newPos := rs.cursorPositions[rs.cursorPosIndex]
	rs.selectedOptionCursor.SetPos(newPos.x, newPos.y)
}
