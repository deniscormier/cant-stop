package stringutil

import (
	"fmt"
	"sort"
	"strconv"
)

type Set map[string]struct{}

func NewStringSet(strings []string) Set {
	set := Set{}
	for _, str := range strings {
		set[str] = struct{}{}
	}
	return set
}

func (s Set) Add(str string) {
	s[str] = struct{}{}
}

func (s Set) Contains(str string) bool {
	_, isAvailable := s[str]
	return isAvailable
}

func (s Set) String() string {
	intArray := s.toSortedIntArray()

	return fmt.Sprintf("%v", intArray)
}

func (s Set) toSortedIntArray() []int {
	tracks := make([]int, 0, len(s))
	for track, _ := range s {
		trackInt, err := strconv.Atoi(track)
		if err != nil {
			fmt.Printf("Failed to convert track from string to int: %s\n", err)
		} else {
			tracks = append(tracks, trackInt)
		}
	}
	sort.Ints(tracks)
	return tracks
}
