package board

import (
	"bitbucket.org/deniscormier/cant-stop/dicecollection"
	"bitbucket.org/deniscormier/cant-stop/markerstate"
	"bitbucket.org/deniscormier/cant-stop/player"
	"bitbucket.org/deniscormier/cant-stop/stringutil"

	"fmt"
	"strconv"
)

type Board struct {
	players        []*player.Player
	currentPlayer  *player.Player
	diceCollection *dicecollection.DiceCollection
}

func NewBoard(playerNames []string) *Board {
	//if players < 1 {
	// Bad news!
	//}

	b := new(Board)

	b.players = make([]*player.Player, 0, len(playerNames))
	for i, name := range playerNames {
		b.players = append(b.players, player.NewPlayer(i, name))
	}

	b.currentPlayer = b.players[0]
	b.diceCollection = dicecollection.NewDiceCollection()

	return b
}

func (b *Board) GetPlayersNames() map[int]string {
	idToName := map[int]string{}

	for _, player := range b.players {
		idToName[player.GetID()] = player.GetName()
	}

	return idToName
}

func (b *Board) PrintDiceCollection() {
	fmt.Println(b.diceCollection)
}

// Get every player's progress. Return a map of player IDs with their state.
func (b *Board) GetPlayersProgress() (playersProgress map[int]markerstate.MarkerState, turnDeltas map[int]markerstate.MarkerState) {
	playersProgress = map[int]markerstate.MarkerState{}
	turnDeltas = map[int]markerstate.MarkerState{}

	for _, player := range b.players {
		playersProgress[player.GetID()] = player.GetProgressWithoutTurnDelta()
		turnDeltas[player.GetID()] = player.GetProgressTurnDelta()
	}

	return playersProgress, turnDeltas
}

func (b *Board) GetCurrentPlayer() *player.Player {
	return b.currentPlayer
}

func (b *Board) GetCurrentPlayerID() int {
	return b.currentPlayer.GetID()
}

// Return the set of dice rolled
func (b *Board) GetCurrentDice() []int {
	return b.diceCollection.GetDiceResults()
}

// You always have the option to roll.
// If your roll, one of the possible 2-dice sums needs to hit at least...
// * one non-completed tracks
// * one of three values you've been accumulating this turn
// ...else, you lose your turn's progress.
// Return false if the roll results in losing the turn. Otherwise, return true.
func (b *Board) CurrentPlayerRoll() bool {
	b.diceCollection.Reset()
	b.diceCollection.Roll()

	unavailableTracks := b.GetCompletedTracks()
	availableTracks := b.currentPlayer.GetAvailableTracks(unavailableTracks)
	possibleSums := b.diceCollection.GetPossibleSums()

	// Check if player loses their turn
	// foreach sum in diceCollection.GetPossibleSums()
	//     is sum in available tracks?
	//         we found a value that can be applied to advance a track! Return set of dice.
	// else, we lost our turn. currentPlayer.LoseTurn(), return nil
	for _, sumInt := range possibleSums {
		sum := strconv.Itoa(sumInt)

		if availableTracks.Contains(sum) {
			return true
		}
	}

	b.currentPlayer.LoseTurn()
	return false
}

// Passing in the dice group selection, add the summed values to this turn's
// progress if at least one of the possible 2-dice sums hits one of three values you've been accumulating this turn.
// Example call: player.ApplyDiceGroups([][]int{
// []int{0, 2},
// []int{1, 3},
// })
// means that we're passing in the 0th + 2nd dice as a group, and the 1st + 3rd dice as another group.
// Return whether the application of groups was successful (with a modification to turnDelta) or not (no modification to turnDelta).
func (b *Board) CurrentPlayerApplyDiceGroups(diceIndices [][]int) []string {
	b.diceCollection.GroupDice(diceIndices)

	sumsAsInt := b.diceCollection.GetSelectionSums()

	sums := []string{}
	for _, sumInt := range sumsAsInt {
		sum := strconv.Itoa(sumInt)

		sums = append(sums, sum)
	}

	return b.currentPlayer.ApplySums(sums, b.GetCompletedTracks())
}

// You always have the option to stop rolling and "cash in" the progress you've made during the turn.
func (b *Board) CurrentPlayerCashIn() {
	b.currentPlayer.ApplyTurnDeltaToProgress()
}

func (b *Board) NextPlayer() {
	// Reset delta in case a turn was in progress and not resolved.
	// We'd rather move on in a known state.
	b.currentPlayer.LoseTurn()

	nextPlayerID := (b.currentPlayer.GetID() + 1) % len(b.players)
	b.currentPlayer = b.players[nextPlayerID]
}

// Query everyone to get all completed tracks
func (b *Board) GetCompletedTracks() stringutil.Set {
	everyoneCompletedTracks := stringutil.Set{}

	for _, player := range b.players {
		playerCompletedTracks := player.GetCompletedTracks()
		for key := range playerCompletedTracks {
			everyoneCompletedTracks.Add(key)
		}
	}

	return everyoneCompletedTracks
}
