package board_test

import (
	"bitbucket.org/deniscormier/cant-stop/board"
	"bitbucket.org/deniscormier/cant-stop/markerstate"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Board", func() {

	var b *board.Board

	Context("A new Board (1 player)", func() {

		BeforeEach(func() {
			b = board.NewBoard([]string{"Edwin"})
		})

		It("Should have player with correct id and name", func() {
			Expect(b.GetPlayersNames()).To(Equal(map[int]string{0: "Edwin"}))
		})

		It("Should have the one player be the current player", func() {
			Expect(b.GetCurrentPlayerID()).To(Equal(0))
		})

		It("Should have default player progress", func() {
			progress, delta := b.GetPlayersProgress()
			Expect(progress).To(Equal(map[int]markerstate.MarkerState{0: markerstate.ZeroMarkerState}))
			Expect(delta).To(Equal(map[int]markerstate.MarkerState{0: markerstate.ZeroMarkerState}))
		})

		It("Should return true after the first roll", func() {
			ok := b.CurrentPlayerRoll()
			Expect(ok).To(BeTrue())
		})

		It("Should return no dice before the first roll", func() {
			dice := b.GetCurrentDice()
			Expect(dice).To(BeNil())
		})

		It("Should return some dice after the first roll", func() {
			b.CurrentPlayerRoll()
			dice := b.GetCurrentDice()
			Expect(len(dice)).To(Equal(4))
		})

		It("Should return sums in the form of strings after applying dice rolls", func() {
			b.CurrentPlayerRoll()
			diceSums := b.CurrentPlayerApplyDiceGroups([][]int{[]int{0, 2}, []int{1, 3}})
			Expect(len(diceSums)).To(Equal(2))
		})

		It("Should change the delta markerstate after applying dice rolls", func() {
			b.CurrentPlayerRoll()
			b.CurrentPlayerApplyDiceGroups([][]int{[]int{0, 2}, []int{1, 3}})

			progress, delta := b.GetPlayersProgress()
			Expect(progress).To(Equal(map[int]markerstate.MarkerState{0: markerstate.ZeroMarkerState}))
			Expect(delta).NotTo(Equal(map[int]markerstate.MarkerState{0: markerstate.ZeroMarkerState}))
		})

		It("Should change the progress markerstate after applying dice rolls, then stopping (cashing in)", func() {
			b.CurrentPlayerRoll()
			b.CurrentPlayerApplyDiceGroups([][]int{[]int{0, 2}, []int{1, 3}})
			b.CurrentPlayerCashIn()

			progress, delta := b.GetPlayersProgress()
			Expect(progress).NotTo(Equal(map[int]markerstate.MarkerState{0: markerstate.ZeroMarkerState}))
			Expect(delta).To(Equal(map[int]markerstate.MarkerState{0: markerstate.ZeroMarkerState}))
		})

		It("Should be a no-op if we run NextPlayer() with 1 player", func() {
			b.CurrentPlayerRoll()
			b.CurrentPlayerApplyDiceGroups([][]int{[]int{0, 2}, []int{1, 3}})
			b.CurrentPlayerCashIn()
			b.NextPlayer()

			Expect(b.GetCurrentPlayerID()).To(Equal(0))
		})

	})

	Context("A new Board (4 players)", func() {

		BeforeEach(func() {
			b = board.NewBoard([]string{"Edwin", "Julie", "Robert", "Lisa"})
		})

		It("Should have player with correct id and name", func() {
			Expect(b.GetPlayersNames()).To(Equal(map[int]string{0: "Edwin", 1: "Julie", 2: "Robert", 3: "Lisa"}))
		})

		It("Should change to next player when running NextPlayer()", func() {
			b.CurrentPlayerRoll()
			b.CurrentPlayerApplyDiceGroups([][]int{[]int{0, 2}, []int{1, 3}})
			b.CurrentPlayerCashIn()
			b.NextPlayer()

			Expect(b.GetCurrentPlayerID()).To(Equal(1))
		})

	})
})
