package dice_test

import (
	"bitbucket.org/deniscormier/cant-stop/dicecollection/dice"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Dice", func() {

	var die *dice.Die

	BeforeEach(func() {
		die = dice.NewDie()
	})

	Context("Rolling once", func() {

		It("Should always be 0 after a reset", func() {
			die.Roll()
			die.Reset()
			r := die.Result()
			Expect(r).To(BeZero())
		})

	})

	Context("Rolling 500 times", func() {

		It("Should always have a result between 1 and 6 (undeterministic)", func() {
			for i := 0; i < 500; i++ {
				die.Roll()
				r := die.Result()
				Expect(r >= 1 && r <= 6).To(BeTrue())
				die.Reset()
			}
		})

		It("Should hit all values from 1 to 6 (undeterministic)", func() {
			valuesRolled := make(map[int]bool)

			for i := 0; i < 500; i++ {
				die.Roll()
				valuesRolled[die.Result()] = true
				die.Reset()
			}

			Expect(len(valuesRolled)).To(Equal(6))
			for i := 1; i <= 6; i++ {
				val, ok := valuesRolled[i]
				Expect(val).To(BeTrue())
				Expect(ok).To(BeTrue())
			}
		})

	})

})
