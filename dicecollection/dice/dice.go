package dice

import (
	"math/rand"
)

const dieMaxValue = 6

type Die struct {
	result int
}

func NewDie() *Die {
	return new(Die)
}

func (d *Die) Reset() {
	d.result = 0
}

func (d *Die) Roll() {
	d.result = rand.Intn(dieMaxValue) + 1
}

func (d *Die) Result() int {
	return d.result
}
