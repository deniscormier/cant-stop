package dice_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"testing"
)

func TestDice(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Dice Suite")
}
