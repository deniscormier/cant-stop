package dicecollection

import (
	"bitbucket.org/deniscormier/cant-stop/dicecollection/dice"

	"fmt"
	"sort"
)

const numDice = 4
const numGroups = 2
const numPossibleSums = 6

type DiceCollection struct {
	diceResults   []int
	selectionSums []int
}

func NewDiceCollection() *DiceCollection {
	dc := new(DiceCollection)

	return dc
}

// Try to return the sums. If not available, return the dice.
func (dc *DiceCollection) String() string {
	var toPrint []int

	if dc.selectionSums != nil {
		toPrint = dc.selectionSums
	} else if dc.diceResults != nil {
		toPrint = dc.diceResults
	} else {
		toPrint = []int{}
	}

	return fmt.Sprintf("%v", toPrint)
}

func (dc *DiceCollection) Reset() {
	dc.diceResults = nil
	dc.selectionSums = nil
}

func (dc *DiceCollection) Roll() {
	dc.diceResults = make([]int, 0, numDice)
	for i := 0; i < numDice; i++ {
		newDie := dice.NewDie()
		newDie.Roll()
		dc.diceResults = append(dc.diceResults, newDie.Result())
	}
}

func (dc *DiceCollection) GetDiceResults() []int {
	return dc.diceResults
}

// Example call: diceCollection.GroupDice([][]int{
// []int{0, 2},
// []int{1, 3},
// })
// means that we're passing in the 0th + 2nd dice as a group, and the 1st + 3rd dice as another group
func (dc *DiceCollection) GroupDice(diceIndices [][]int) {
	if !isValidDiceIndices(diceIndices) {
		return
	}

	dc.selectionSums = make([]int, 0, numGroups)

	// For each group of indices
	for i := 0; i < numGroups; i++ {
		groupSum := 0

		// For each index in the group, sum values
		for j := 0; j < len(diceIndices[i]); j++ {
			index := diceIndices[i][j]
			groupSum += dc.diceResults[index]
		}

		dc.selectionSums = append(dc.selectionSums, groupSum)
	}
}

func (dc *DiceCollection) GetSelectionSums() []int {
	return dc.selectionSums
}

func (dc *DiceCollection) GetPossibleSums() []int {
	if dc.diceResults == nil {
		return nil
	}

	possibleSums := make([]int, 0, numPossibleSums)

	// Cover all permutations of putting 2 results together
	for i := 0; i < len(dc.diceResults)-1; i++ {
		for j := i + 1; j < len(dc.diceResults); j++ {
			possibleSums = append(possibleSums, dc.diceResults[i]+dc.diceResults[j])
		}
	}
	return possibleSums
}

// Validate that the provided indices can be interpreted as a grouping.
func isValidDiceIndices(diceIndices [][]int) bool {
	// Confirm that we have 2 arrays of 2 elements.
	// Then, flatten arrays and find indices 0, 1, 2, 3.

	if len(diceIndices) != 2 {
		return false
	}

	if len(diceIndices[0]) != 2 || len(diceIndices[1]) != 2 {
		return false
	}

	flatArray := []int{diceIndices[0][0], diceIndices[0][1], diceIndices[1][0], diceIndices[1][1]}
	sort.Ints(flatArray)

	return flatArray[0] == 0 &&
		flatArray[1] == 1 &&
		flatArray[2] == 2 &&
		flatArray[3] == 3
}
