package dicecollection_test

import (
	"bitbucket.org/deniscormier/cant-stop/dicecollection"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

// func NewDiceCollection() *DiceCollection
// func (dc *DiceCollection) Reset()
// func (dc *DiceCollection) Roll()
// func (dc *DiceCollection) GetDiceResults() []int
// func (dc *DiceCollection) GroupDice(diceIndices [][]int)
// func (dc *DiceCollection) GetSelectionSums() []int
// func (dc *DiceCollection) GetPossibleSums() []int

var _ = Describe("Dice Collection", func() {

	var diceCollection *dicecollection.DiceCollection

	BeforeEach(func() {
		diceCollection = dicecollection.NewDiceCollection()
	})

	Context("Rolling a collection once", func() {

		BeforeEach(func() {
			diceCollection.Roll()
		})

		It("Dice should be non-zero", func() {
			diceResults := diceCollection.GetDiceResults()

			numDice := 4
			Expect(len(diceResults)).To(Equal(numDice))
			for i := 0; i < numDice; i++ {
				Expect(diceResults[i]).NotTo(BeZero())
			}
		})

		It("Possible sums should consist of all permutations", func() {
			diceResults := diceCollection.GetDiceResults()
			possibleSums := diceCollection.GetPossibleSums() // NOTE: Current implementation returns multiple ints of the same value

			// NOTE: Checks are loose given that we would be satisfied with just getting the unique values

			// At a minimum, we have one possible sum, and it consists of 2 dice results joined together
			pSumsLen := len(possibleSums)
			Expect(pSumsLen > 0).To(BeTrue())
			for i := 0; i < pSumsLen; i++ {
				ps := possibleSums[i]
				Expect(ps >= 2 && ps <= 12).To(BeTrue())
			}

			// For all permutations of selecting 2 results, the value should show up in possible sums
			Expect(possibleSums).To(ContainElement(diceResults[0] + diceResults[1]))
			Expect(possibleSums).To(ContainElement(diceResults[0] + diceResults[2]))
			Expect(possibleSums).To(ContainElement(diceResults[0] + diceResults[3]))
			Expect(possibleSums).To(ContainElement(diceResults[1] + diceResults[2]))
			Expect(possibleSums).To(ContainElement(diceResults[1] + diceResults[3]))
			Expect(possibleSums).To(ContainElement(diceResults[2] + diceResults[3]))
		})

	})

	Context("Rolling a collection once, then resetting", func() {

		BeforeEach(func() {
			diceCollection.Roll()
			diceCollection.Reset()
		})

		It("Dice should be nil", func() {
			diceResults := diceCollection.GetDiceResults()
			Expect(diceResults).To(BeZero())
		})

		It("Possible sums should be nil", func() {
			possibleSums := diceCollection.GetPossibleSums()
			Expect(possibleSums).To(BeZero())
		})

	})

	Context("Rolling a collection once and setting groups", func() {

		BeforeEach(func() {
			diceCollection.Roll()
			diceCollection.GroupDice([][]int{
				[]int{0, 2},
				[]int{1, 3},
			})
		})

		It("Dice should be non-zero", func() {
			diceResults := diceCollection.GetDiceResults()

			numDice := 4
			Expect(len(diceResults)).To(Equal(numDice))
			for i := 0; i < numDice; i++ {
				Expect(diceResults[i]).NotTo(BeZero())
			}
		})

		It("Sums should consist of dice by index [(0, 2), (1, 3)]", func() {
			diceResults := diceCollection.GetDiceResults()
			sums := diceCollection.GetSelectionSums()

			Expect(len(sums)).To(Equal(2))
			Expect(sums[0]).To(Equal(diceResults[0] + diceResults[2]))
			Expect(sums[1]).To(Equal(diceResults[1] + diceResults[3]))
		})

	})

	Context("Rolling a collection once, setting groups, and resetting", func() {

		BeforeEach(func() {
			diceCollection.Roll()
			diceCollection.GroupDice([][]int{
				[]int{0, 2},
				[]int{1, 3},
			})
			diceCollection.Reset()
		})

		It("Dice should be nil", func() {
			diceResults := diceCollection.GetDiceResults()
			Expect(diceResults).To(BeZero())
		})

		It("Sums should be nil", func() {
			sums := diceCollection.GetSelectionSums()
			Expect(sums).To(BeZero())
		})

	})

	Context("Rolling a collection once, incorrect setting (duplicate index)", func() {

		BeforeEach(func() {
			diceCollection.Roll()
			diceCollection.GroupDice([][]int{
				[]int{0, 1},
				[]int{1, 3},
			})
			diceCollection.Reset()
		})

		It("Dice should be nil", func() {
			diceResults := diceCollection.GetDiceResults()
			Expect(diceResults).To(BeZero())
		})

		It("Sums should be nil", func() {
			sums := diceCollection.GetSelectionSums()
			Expect(sums).To(BeZero())
		})

	})

	Context("Rolling a collection once, incorrect setting (3 indices in one array, one in the other)", func() {

		BeforeEach(func() {
			diceCollection.Roll()
			diceCollection.GroupDice([][]int{
				[]int{0, 1, 2},
				[]int{3},
			})
			diceCollection.Reset()
		})

		It("Dice should be nil", func() {
			diceResults := diceCollection.GetDiceResults()
			Expect(diceResults).To(BeZero())
		})

		It("Sums should be nil", func() {
			sums := diceCollection.GetSelectionSums()
			Expect(sums).To(BeZero())
		})

	})

	Context("Rolling a collection once, incorrect setting (index > 3)", func() {

		BeforeEach(func() {
			diceCollection.Roll()
			diceCollection.GroupDice([][]int{
				[]int{0, 1},
				[]int{2, 4},
			})
			diceCollection.Reset()
		})

		It("Dice should be nil", func() {
			diceResults := diceCollection.GetDiceResults()
			Expect(diceResults).To(BeZero())
		})

		It("Sums should be nil", func() {
			sums := diceCollection.GetSelectionSums()
			Expect(sums).To(BeZero())
		})

	})

})
