package dicecollection_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"testing"
)

func TestDiceCollection(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Dice Collection Suite")
}
